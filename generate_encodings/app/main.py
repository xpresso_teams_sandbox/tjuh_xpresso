"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger


import os
import json
import pandas as pd
import shutil
import torch
import dill
import spacy
import en_core_web_sm
from pytorch_transformers import *

from collections import defaultdict
from scipy.spatial.distance import cosine

sp = en_core_web_sm.load()
all_stopwords = sp.Defaults.stop_words


__author__ = "### Author ###"

logger = XprLogger("generate_encodings",level=logging.INFO)


class GenerateEncodings(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """
    # MODEL_DATA_NAME = 'corpus.txt'

    MODEL_DATA_NAME = "model_data"

    def __init__(self):
        super().__init__(name="GenerateEncodings")
        """ Initialize all the required constansts and data her """


        # self.model_dir =  "/data/" + self.MODEL_DATA_NAME
        self.model_dir =  "/data/" + self.MODEL_DATA_NAME
        # self.model_dir =  "../data/" + self.MODEL_DATA_NAME

        print(self.model_dir )

        logger.info("List of files present in pulled folder : {}".format(
            os.listdir(self.model_dir)))
        logger.info("Moving files from {} to {}".format(self.model_dir,
                                                        self.OUTPUT_DIR))
        # shutil.copy(self.model_dir,self.OUTPUT_DIR)
        shutil.move(self.model_dir,self.OUTPUT_DIR)

        logger.info("Contents of {} after moving files is {}".format(
            self.OUTPUT_DIR,os.listdir(self.OUTPUT_DIR)))


        self.model_name_or_path = self.OUTPUT_DIR
        # self.model_name_or_path = '../data/BioClinicalBERT'


        self.dict_name = os.path.join(self.OUTPUT_DIR,
                                      'vec_dict.pkl')

        self.doc_id_doc_dict_loc = os.path.join(self.OUTPUT_DIR,
                                      'doc_id_doc_dict.json')

        self.sen_id_sen_dict_loc = os.path.join(self.OUTPUT_DIR,
                                                'sen_id_sen_dict.json')

        self.sen_id_doc_id_dict_loc = os.path.join(self.OUTPUT_DIR,
                                                'sen_id_doc_id_dict.json')

        self.doc_id_sen_dict_loc =  os.path.join(self.OUTPUT_DIR,
                                                'doc_id_sen_dict.json')



        # self.dict_name = os.path.join('../data/',
        #                               'vec_dict.pkl')
        #
        # self.doc_id_doc_dict_loc = os.path.join('../data/',
        #                               'doc_id_doc_dict.json')
        #
        # self.sen_id_sen_dict_loc = os.path.join('../data/',
        #                                         'sen_id_sen_dict.json')
        #
        # self.sen_id_doc_id_dict_loc = os.path.join('../data/',
        #                                         'sen_id_doc_id_dict.json')
        #
        # self.doc_id_sen_dict_loc =  os.path.join('../data/',
        #                                         'doc_id_sen_dict.json')


        logger.info("Model path : {} and embedding_json_path {}".format(
            self.model_name_or_path,self.dict_name))

        self.model_class = BertModel
        self.tokenizer_class = BertTokenizer
        self.tokenizer = self.tokenizer_class.from_pretrained(
            self.model_name_or_path)
        self.model = self.model_class.from_pretrained(self.model_name_or_path)

        self.doc_data = None

        self.doc_id_doc_dict = defaultdict(lambda: None)

        # for every sentence id in the dictionary its corresponding sentence has been mapped
        self.sen_id_sen_dict = defaultdict(lambda: None)

        # for every sentence id in the dictionary its document id(document to which the sentence belongs) has been mapped
        self.sen_id_doc_id_dict = defaultdict(lambda: None)

        # for every document id in the dictionary its sentence (document to which the sentence belongs) has been mapped
        self.doc_id_sen_dict = defaultdict(lambda: None)

        # for every sentence id it's generated embedding are mapped
        self.sen_id_embed_dict = defaultdict(lambda: None)






    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===

            # Assigning document_id to documents and storing the doc content against

            # reading documents
            self.doc_data = pd.read_csv(os.path.join(self.OUTPUT_DIR,'corpus.txt'), sep='\n', header=None)
            # self.doc_data = pd.read_csv(os.path.join(self.model_dir), sep='\n', header=None)

            self.doc_data.columns = ['document']

            for index in range(len(self.doc_data)):
                self.doc_id_doc_dict[index] = self.doc_data['document'][index]

            import re
            from spacy.lang.en import English
            # nlp = English()
            # os.system('!python -m spacy download en_core_web_sm')
            nlp = spacy.load('en_core_web_sm')

            # sbd = nlp.create_pipe('sentencizer')
            # nlp.add_pipe(sbd)

            sent_id = 0

            # iterating over the dictionary to create sentences
            for doc_id, document in self.doc_id_doc_dict.items():

                # for storing sentences
                sents_list = []

                document = re.sub(r"[\u2022]+", "", document)
                document = re.sub(r"[~|_|\-]+", "", document)
                document = re.sub(r"\[+ \]+", "", document)
                document = document.strip()
                # document = re.sub(r"  ",""document)

                doc = nlp(document)
                # with open('generate_sent_corpus_'+str(starting_index)+'-'+str(ending_index)+'.txt','a') as sent_f:

                # iterating over each sentence to generate dictionaries
                for sent in doc.sents:
                    if len(sent.text) <= 10 or len(sent.text.split()) <= 3:
                        pass
                    else:
                        sents_list_ids = []

                        # to assign sentence id
                        sent_id += 1

                        if sent.text[len(sent.text) - 1] == '.' and sent.text[len(sent.text) - 2].isnumeric() and \
                                ( sent.text[len(sent.text) - 3] == ' '  or (sent.text[len(sent.text)-3].isnumeric() and sent.text[len(sent.text)-4]== ' ')):
                            docz = sent.text[:len(sent.text) - 4].strip()

                            logger.info(docz)
                            print("sent_id:",sent_id)
                            self.sen_id_sen_dict[sent_id] = docz

                            # every sentence_id against its document id
                            self.sen_id_doc_id_dict[sent_id] = doc_id

                            sents_list.append(docz.strip())

                        else:
                            # print(sent.text)
                            # assigning sentence_id to every valid sentence
                            self.sen_id_sen_dict[sent_id] = sent.text.strip()

                            # every sentence_id against its document id
                            self.sen_id_doc_id_dict[sent_id] = doc_id

                            sents_list.append(sent.text.strip())

                            # every doc_d against list of sentences in that document
                        self.doc_id_sen_dict[doc_id] = sents_list

                        # # assigning sentence_id to every valid sentence
                        # self.sen_id_sen_dict[sent_id] = sent.text.strip()
                        #
                        # # every sentence_id against its document id
                        # self.sen_id_doc_id_dict[sent_id] = doc_id
                        #
                        # sents_list.append(sent.text.strip())
                        #
                        # # every doc_d against list of sentences in that document
                        # self.doc_id_sen_dict[doc_id] = sents_list

                # print(key)


            logger.info("Successfully extracted sentences and created dictionaries"
                        " Creating sentence embedding")

            self.sen_id_embed_dict = self.get_token_dict(self.sen_id_sen_dict,self.tokenizer,
                                           self.model)

            logger.info("Saving sentence embedding into {}".format(self.sen_id_embed_dict.__len__() ))


            with open(self.dict_name, "wb") as f:
                    dill.dump(self.sen_id_embed_dict, f, protocol=4)

            j = json.dumps(self.sen_id_sen_dict)
            j1 = json.dumps(self.doc_id_doc_dict)
            j2 = json.dumps(self.sen_id_doc_id_dict)
            j3 = json.dumps(self.doc_id_sen_dict)



            with open(self.sen_id_sen_dict_loc,'w') as f:
                f.write(j)
            with open(self.doc_id_doc_dict_loc,'w') as f1:
                f1.write(j1)
            with open(self.sen_id_doc_id_dict_loc, 'w') as f2:
                f2.write(j2)
            with open(self.doc_id_sen_dict_loc,'w') as f3:
                f3.write(j3)



            logger.info("Process completed")

            logger.info("List of contents in {} after saving embedding json : {}".format(self.OUTPUT_DIR,os.listdir(self.OUTPUT_DIR)))



        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed()

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=True, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp)

        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


    @staticmethod
    def get_token_dict(inp_list, tokenizer, model):
        count = 0
        v_dict = defaultdict(lambda: None)
        # defaultdict(lambda: defaultdict(lambda : None))
        # for word in inp_list:
        for sen_id, sen in inp_list.items():
            # for word in inp_list:
            count += 1
            print(count)
            with torch.no_grad():
                try:
                    inp_id = torch.tensor(
                        [tokenizer.encode(sen, add_special_tokens=True)])
                    vec = model(inp_id)[0][0]
                    logger.info("count:", count)
                    vec_dim = torch.mean(vec, dim=0)
                    v_dict[sen_id] = vec_dim
                except:
                    logger.info(sen)
        return v_dict



if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = GenerateEncodings()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
