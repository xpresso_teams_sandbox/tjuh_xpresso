"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""

import logging
from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger

import os
import  json
import heapq
import torch
import dill
# from flask import jsonify
from pytorch_transformers import *
from collections import defaultdict
from scipy.spatial.distance import cosine



__author__ = "### Author ###"

logger = XprLogger("inference",level=logging.INFO)


class Inference(AbstractInferenceService):
    """ Main class for the inference service. User will need to implement
    following functions:
       - load_model: It gets the directory of the model stored as parameters,
           user will need to implement the method for loading the model and
           updating self.model variable
       - transform_input: It gets the JSON object from the rest API as input.
           User will need to implement the method to convert the json data to
           relevant feature vector to be used for prediction
       - predict: It gets the feature vector or any other object from the
           transform_input method. User will need to implement the model
           prediction codebase here. It returns the predicted value
       - transform_output: It gets the predicted value from the predict method.
           User need to implement this method to converted predicted method
           to JSON serializable object. Response from this method is send back
           to the client as JSON Object.

    AbstractInferenceService automatically creates the flask reset api
    with resource /predict for this class.
    Request JSON format:
       {
         "input" : <input_goes_here> // It could be any JSON object
       }
       This value of "input" key is sent to transform_input

    Response JSON format:
       {
         "message": "success/failure",
         "results": <response goes here> // It could be any JSOn object
       }
       Output of transform_output goes as value of "results"
    """

    REQUEST_TEXT_KEY = "input_sen"
    REQUEST_TOP_K_KEY = "topk"


    def __init__(self):
        super().__init__()
        """ Initialize any static data required during boot up """

        self.vec_dict = None

        self.sen_id_sen_dict = {}
        self.sen_id_doc_id_dict = {}
        self.doc_id_sen_dict = {}
        self.doc_id_doc_dict = {}

        self.inp_vec_dim = None
        self.model_class = BertModel
        self.tokenizer_class = BertTokenizer



    def load_model(self, model_path):
        """ This is used to load the model on the boot time.
        User will need to load the model and save it in the variable
        self.model. It is IMPORTANT to update self.model.
        Args:
            model_path(str): Path of the directory where the model files are
               stored.
        """
        self.model = self.model_class.from_pretrained(model_path)
        self.tokenizer = self.tokenizer_class.from_pretrained(model_path)


        with open(os.path.join(model_path,'vec_dict.pkl'), "rb") as f:
            self.vec_dict = dill.load(f)

        with open(os.path.join(model_path,'sen_id_sen_dict.json'), "r") as f1:
            temp_sen_id_sen_dict = json.load(f1)

        with open(os.path.join(model_path, 'sen_id_doc_id_dict.json'), "r") as f2:
            temp_sen_id_doc_id_dict= json.load(f2)

        with open(os.path.join(model_path, 'doc_id_sen_dict.json'), "r") as f3:
            temp_doc_id_sen_dict= json.load(f3)

        with open(os.path.join(model_path, 'doc_id_doc_dict.json'), "r") as f4:
            temp_doc_id_doc_dict= json.load(f4)


        # with open(os.path.join('../data/','vec_dict.pkl'), "rb") as f:
        #     self.vec_dict = dill.load(f)
        #
        # with open(os.path.join('../data/','sen_id_sen_dict.json'), "r") as f1:
        #     temp_sen_id_sen_dict = json.load(f1)
        #
        # with open(os.path.join('../data/', 'sen_id_doc_id_dict.json'), "r") as f2:
        #     temp_sen_id_doc_id_dict= json.load(f2)
        #
        # with open(os.path.join('../data/', 'doc_id_sen_dict.json'), "r") as f3:
        #     temp_doc_id_sen_dict= json.load(f3)
        #
        # with open(os.path.join('../data/', 'doc_id_doc_dict.json'), "r") as f4:
        #     temp_doc_id_doc_dict= json.load(f4)




        for keys,value in temp_doc_id_doc_dict.items():
            self.doc_id_doc_dict[int(keys)] = value

        for keys, value in temp_sen_id_sen_dict.items():
            self.sen_id_sen_dict[int(keys)] = value

        for keys, value in temp_doc_id_sen_dict.items():
            self.doc_id_sen_dict[int(keys)] = value

        for keys, value in temp_sen_id_doc_id_dict.items():
            self.sen_id_doc_id_dict[int(keys)] = value

        pass

    def transform_input(self, input_request):
        """
        Convert the raw input request to a feature data or any other object
        to be used during prediction

        Args:
            input_request: JSON Object or an array received from the REST API,
               which needs to be converted into relevant feature data.

        Returns:
            obj: Any transformed object
        """

        logger.info("Calling transform_input with {}".format(input_request))

        input_word = input_request[self.REQUEST_TEXT_KEY]
        with torch.no_grad():
            input_ids = torch.tensor(
                [self.tokenizer.encode(input_word, add_special_tokens=True)])
            inp_vec = self.model(input_ids)[0][0]
            self.inp_vec_dim = torch.mean(inp_vec, dim=0)
            print(self.inp_vec_dim.shape)

        logger.info("Exiting transform_input with return value {}".format(input_request))
        return input_request

        pass

    def predict(self, input_request):
        """
        This method implements the prediction method of the supported model.
        It gets the output of transform_input as input and returns the predicted
        value

        Args:
            input_request: Transformed object outputted from transform_input
               method

        Returns:
            obj: predicted value from the model

        """

        logger.info("Calling predict with {}".format(input_request))

        sim_dict = self.get_sim_dict()
        if self.REQUEST_TOP_K_KEY in input_request.keys():

            # results = heapq.nlargest(input_request[self.REQUEST_TOP_K_KEY], sim_dict,
            #                          key=sim_dict.get)
            unique_sentences, unique_scores, unique_sentence_ids = self.get_top_k_unique_sentences(input_request[self.REQUEST_TOP_K_KEY], sim_dict,
                                                                                                   self.sen_id_sen_dict)

        else:
            # results = heapq.nlargest(20, sim_dict,key=sim_dict.get)
            unique_sentences, unique_scores, unique_sentence_ids = self.get_top_k_unique_sentences(20, sim_dict,
                                                                                                   self.sen_id_sen_dict)

        # unique_sentences, unique_scores, unique_sentence_ids = self.get_top_k_unique_sentences(20, sim_dict, self.sen_id_sen_dict)

        logger.info('Unique sentneces generated... getting doc mapping')
        document_ids = self.get_document_ids(unique_sentence_ids)

        res = []
        for i in range(len(unique_sentences)):
            res.append(
                {'sentence': unique_sentences[i], 'scores': unique_scores[i], 'sentence_id': unique_sentence_ids[i],
                 'document_id': document_ids[i]})
        logger.info('Everything completed')


        return res


    def transform_output(self, output_response):
        """
        Convert the predicted value into a relevant JSON serializable object.
        This is sent back to the REST API call
        Args:
            output_response: Predicted output from predict method.

        Returns:
          obj: JSON Serializable object
        """
        logger.info("Transforming {} in transform_output".format(output_response))
        response = output_response
        logger.info("Sending response : {} back".format(response))
        return response

    # getting similarity between 2 words using cosine similarity
    def get_sim_dict(self):
        sim_dict = defaultdict(lambda: None)
        for word, s_v in self.vec_dict.items():
            # print("word is ",word)
            sim_score = 1 - cosine(self.inp_vec_dim, s_v)
            # print("sim_Score is ",sim_score)
            sim_dict[word] = sim_score

        return sim_dict

    def fetch_unique_sen(self, sim_dict, sentence_id_list, number_of_sent, sen_id_sen_dict):

        top_k_sentences = []
        top_k_sentence_idx = []
        top_k_scores = []

        unique_sen_ids = []
        unique_sentences = []

        for id in sentence_id_list:
            # print("score: "+ str(sim_dict[id])+"      "+str(sen_id_sen_dict[id]))
            top_k_sentences.append(sen_id_sen_dict[id])
            top_k_scores.append(sim_dict[id])

        # unique_sentences = [x for i, x in enumerate(top_k_sentences) if i == top_k_sentences.index(x)]

        for i, x in enumerate(top_k_sentences):
            if i == top_k_sentences.index(x):
                unique_sentences.append(x)
                top_k_sentence_idx.append(i)

        for i in range(len(top_k_sentence_idx)):
            unique_sen_ids.append(sentence_id_list[top_k_sentence_idx[i]])

        # print(unique_sen_ids)

        unique_scores = [x for i, x in enumerate(top_k_scores) if i == top_k_scores.index(x)]

        return unique_sentences, unique_scores, unique_sen_ids

    def get_top_k_unique_sentences(self, number_of_sent, sim_dict, sen_id_sen_dict):
        # get top similar words
        # import heapq

        number_of_unique_sent = 0

        # print(type(number_of_sent))
        temp_number_of_sent = number_of_sent
        while (number_of_unique_sent < number_of_sent):
            unique_sentences = []
            unique_scores = []
            sentence_id_list = heapq.nlargest(temp_number_of_sent, sim_dict, key=sim_dict.get)
            unique_sentences, unique_scores, unique_sentence_ids = self.fetch_unique_sen(sim_dict, sentence_id_list,
                                                                                         number_of_sent,
                                                                                         sen_id_sen_dict)
            number_of_unique_sent = len(unique_sentences)
            # print("number_of_unique_sent",number_of_unique_sent)

            temp_number_of_sent += 5

        unique_sentences = unique_sentences[:number_of_sent]
        unique_scores = unique_scores[:number_of_sent]
        unique_sentence_ids = unique_sentence_ids[:number_of_sent]

        return unique_sentences, unique_scores, unique_sentence_ids

    # for idx in range(len(unique_sentences)):
    #   print("score:"+str(unique_scores[idx])+"    "+str(unique_sentence_ids[idx])+"   "+unique_sentences[idx])

    def get_document_ids(self, sentence_ids_list):
        doc_ids = []
        for i in sentence_ids_list:
            doc_ids.append(self.sen_id_doc_id_dict[i])
        return doc_ids


if __name__ == "__main__":
    pred = Inference()
    # === To run locally. Use load_model instead of load. ===
    # pred.load_model(model_path="/model_path/") instead of pred.load()
    pred.load()
    pred.run_api(port=5000)
